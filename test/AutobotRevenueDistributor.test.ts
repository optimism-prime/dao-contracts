import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { expect } from "chai";
import hardhat from "hardhat";
import { ethers } from "hardhat";
import { AutobotRevenueDistributor, OppTestERC20 } from "../typechain-types";
import { smock } from "@defi-wonderland/smock";
import { BigNumber } from "ethers";

type TestingContext = {
  owner: SignerWithAddress;
  stranger: SignerWithAddress;
  distributor: AutobotRevenueDistributor;
  oppTestErc20: OppTestERC20;
};

describe("AutobotRevenueDistributor", () => {
  const deployTestingContextFixture = async (): Promise<TestingContext> => {
    const [owner, stranger] = await ethers.getSigners();

    const AutobotRevenueDistributor = await ethers.getContractFactory(
      "AutobotRevenueDistributor",
    );
    const OppTestERC20 = await ethers.getContractFactory("OppTestERC20");

    const distributor = await AutobotRevenueDistributor.deploy();
    const oppTestErc20 = await OppTestERC20.deploy();

    await distributor.deployed();
    await oppTestErc20.deployed();

    return {
      owner,
      stranger,
      distributor,
      oppTestErc20,
    };
  };

  describe("Access", () => {
    it("Should prevent stranger from setting rates", async () => {
      const { stranger, distributor }: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );
      await expect(
        distributor.connect(stranger).setGoldenRate(1),
      ).to.be.revertedWith("Ownable: caller is not the owner");
      await expect(
        distributor.connect(stranger).setLegendaryRate(1),
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });

    it("Should prevent stranger from withdrawing", async () => {
      const { stranger, distributor, oppTestErc20 }: TestingContext =
        await loadFixture(deployTestingContextFixture);
      await expect(
        distributor.connect(stranger).withdraw(oppTestErc20.address),
      ).to.be.revertedWith("Ownable: caller is not the owner");
    });
  });

  describe("Withdraw", () => {
    it("Should allow owner to withdraw", async () => {
      const { owner, distributor, oppTestErc20 }: TestingContext =
        await loadFixture(deployTestingContextFixture);

      const amount = 42_240;
      await oppTestErc20.transfer(distributor.address, amount);
      expect(amount).to.equal(
        await oppTestErc20.balanceOf(distributor.address),
      );
      const myBalance = await oppTestErc20.balanceOf(owner.address);
      await distributor.withdraw(oppTestErc20.address);
      expect(0).to.equal(await oppTestErc20.balanceOf(distributor.address));
      expect(myBalance.toNumber() + amount).to.equal(
        await oppTestErc20.balanceOf(owner.address),
      );
    });
  });

  describe("Deployment", () => {
    it("Should set the correct rates", async () => {
      const { owner, distributor }: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );

      expect(await distributor.owner()).to.equal(owner.address);
      const rateDivider = await distributor.RATE_DIVIDER();
      const goldenRate = await distributor.goldenRate();
      const legendaryRate = await distributor.legendaryRate();
      expect(goldenRate.toNumber() / rateDivider.toNumber()).to.equal(0.75);
      expect(legendaryRate.toNumber() / rateDivider.toNumber()).to.equal(0.25);
    });

    it("Should target 4 legendary NFTs", async () => {
      const { distributor }: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );
      expect(await distributor.legendaryIdsLength()).to.equal(4);
    });

    it("Should target 50 golden NFTs", async () => {
      const { distributor }: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );
      expect(await distributor.goldenIdsLength()).to.equal(50);
    });
  });

  describe("Distribution", () => {
    it("Should distribute to all NFTs holders", async () => {
      const { distributor, oppTestErc20 }: TestingContext = await loadFixture(
        deployTestingContextFixture,
      );
      const autobotsFake = await smock.fake(
        (
          await hardhat.artifacts.readArtifact("IERC721")
        ).abi,
        {
          address: await distributor.AUTOBOTS(),
        },
      );
      autobotsFake.ownerOf.returns(
        (tokenId: BigNumber) => ownerOf[tokenId.toString()],
      );

      // Transfer an amount of token to the distributor and ask it to distribute them to holders
      const amount = 326_504_446;
      await oppTestErc20.transfer(distributor.address, amount);
      await distributor.distribute(oppTestErc20.address);

      const lefthover = await oppTestErc20.balanceOf(distributor.address);
      expect(lefthover).to.be.greaterThan(0);
      // Due to integer arithmetic and large divisions some dust are left in the contract
      // they'll be distributed next time

      const rateDivider = await distributor.RATE_DIVIDER();
      const goldenRate = await distributor.goldenRate();
      const legendaryRate = await distributor.legendaryRate();
      const goldenCount = 50;
      const legendaryCount = 4;

      const expectedPerLegendary = Math.floor(
        Math.floor(
          (amount * legendaryRate.toNumber()) / rateDivider.toNumber(),
        ) / legendaryCount,
      );
      const expectedPerGolden = Math.floor(
        Math.floor((amount * goldenRate.toNumber()) / rateDivider.toNumber()) /
          goldenCount,
      );
      const totalDistributed =
        expectedPerLegendary * legendaryCount + expectedPerGolden * goldenCount;

      console.log(`expectedPerLegendary = ${expectedPerLegendary}`);
      console.log(`expectedPerGolden = ${expectedPerGolden}`);
      console.log(`totalDistributed = ${totalDistributed}`);

      expect(lefthover).to.equal(amount - totalDistributed);

      const expectedBalanceOf: { [key: string]: number } = {};
      for (const tokenId in ownerOf) {
        expectedBalanceOf[ownerOf[tokenId]] = 0;
      }
      for (const tokenId in ownerOf) {
        expectedBalanceOf[ownerOf[tokenId]] += legendaries.includes(tokenId)
          ? expectedPerLegendary
          : expectedPerGolden;
      }

      let sum = 0;
      for (const owner in expectedBalanceOf) {
        const amount = await oppTestErc20.balanceOf(owner);
        sum += amount.toNumber();
        expect(expectedBalanceOf[owner]).equal(amount);
      }
      expect(totalDistributed).to.equal(sum);
    });
  });
});

const legendaries = ["768", "940", "512", "256"];

const ownerOf: { [key: string]: string } = {
  "15": "0x72241b7cd963Cf56ba409E80ED59E1AeE76a1adA",
  "24": "0x6ff3CaD69a3798a703a127CA76915F062DbE9762",
  "62": "0x1bC3e2c44E0b2dabB1b06A1890273571825d3e03",
  "79": "0x7D374fe73fD8358a93c53e978477F7Ba4129BE0C",
  "80": "0x7D374fe73fD8358a93c53e978477F7Ba4129BE0C",
  "85": "0xF4B93a76bfedbaA5DD58C37C438bDe519E881B37",
  "115": "0xb118026c68FD7f03efbcdf675DEA96d625c084e5",
  "169": "0xCD114ebB9c8EAbAA2026160CCaE56a6Bd4103042",
  "189": "0x6A234519F071fe33E2422E87F7Bd1D17F3BDc4c6",
  "210": "0xF4B93a76bfedbaA5DD58C37C438bDe519E881B37",
  "214": "0xF4B93a76bfedbaA5DD58C37C438bDe519E881B37",
  "248": "0xF4B93a76bfedbaA5DD58C37C438bDe519E881B37",
  "256": "0xBd48855db52b9d0B5ea728E75Fb20B74297ac70E",
  "274": "0xF4B93a76bfedbaA5DD58C37C438bDe519E881B37",
  "276": "0xF4B93a76bfedbaA5DD58C37C438bDe519E881B37",
  "282": "0x188C30E9A6527F5F0c3f7fe59B72ac7253C62F28",
  "296": "0x188C30E9A6527F5F0c3f7fe59B72ac7253C62F28",
  "306": "0xF4B93a76bfedbaA5DD58C37C438bDe519E881B37",
  "349": "0x917b4B0E86fC7766695095dD1A5292B3BE8b2D14",
  "396": "0x615e5022252B31AF496574F00D5519C5d93261AB",
  "398": "0x615e5022252B31AF496574F00D5519C5d93261AB",
  "421": "0x9C529Af39499Eec506aC31fDfA13bb299C2C2A55",
  "422": "0x615e5022252B31AF496574F00D5519C5d93261AB",
  "438": "0xcD763667cd74543Bd05c19386347441D3D3f6392",
  "489": "0xB393F2fa4C9df2dA3E54D169d0561D9E5979DfF1",
  "502": "0x5FFd5cec88490D984d3f83496592B65C1225871c",
  "509": "0xFeEfAe7574492D1ED4f4113b564f36bAA5d6E16c",
  "512": "0x7B2fC0feacDf2F59bc26F19839AeB6eeE43f4224",
  "540": "0x2e1C62353efb3743729E1aF6c234b32cC4EfAF85",
  "547": "0xc5cf8D4bef8C4897BB596c8e2a671767AD691964",
  "557": "0x11eBeE2bF244325B5559f0F583722d35659DDcE8",
  "562": "0x49a5492FDFe5AcC966dD5f41310dfDfe8dAA349C",
  "565": "0xFee6be6B5cc8Cb4EE8189850A69973E774e7614e",
  "582": "0x79C80FB4431B23940c0ed3f5ce45ba71d0f03577",
  "590": "0x94E62D9f43F465F7BE6C5Efc2BfE76958F3972a3",
  "609": "0xCD114ebB9c8EAbAA2026160CCaE56a6Bd4103042",
  "637": "0xF4B93a76bfedbaA5DD58C37C438bDe519E881B37",
  "685": "0xCD114ebB9c8EAbAA2026160CCaE56a6Bd4103042",
  "712": "0x6e42f2126d23f9F34bB5b15f70c276bF1EAAbFd4",
  "747": "0xF4B93a76bfedbaA5DD58C37C438bDe519E881B37",
  "768": "0xF4B93a76bfedbaA5DD58C37C438bDe519E881B37",
  "781": "0xF4B93a76bfedbaA5DD58C37C438bDe519E881B37",
  "836": "0x52F8B09FAc8E1B7D7dC45643CA27d0f573814AB8",
  "837": "0x52F8B09FAc8E1B7D7dC45643CA27d0f573814AB8",
  "846": "0x606d2C436A07be40C276a6176bb1376C34e49Ee9",
  "853": "0x30858A8051eaa08FBC3dc37FB290792FD30664b7",
  "905": "0x7D374fe73fD8358a93c53e978477F7Ba4129BE0C",
  "921": "0x1a86596A4F3398B6c44232ba1716e20e88205A40",
  "927": "0x1a86596A4F3398B6c44232ba1716e20e88205A40",
  "935": "0x94E62D9f43F465F7BE6C5Efc2BfE76958F3972a3",
  "937": "0x94E62D9f43F465F7BE6C5Efc2BfE76958F3972a3",
  "938": "0x2A4FD28E59f3c48bC76357D0125966eda217EA11",
  "939": "0x2A4FD28E59f3c48bC76357D0125966eda217EA11",
  "940": "0x2A4FD28E59f3c48bC76357D0125966eda217EA11",
};
