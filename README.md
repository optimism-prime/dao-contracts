# Optimism Prime DAO contracts

This is a repository for contracts implementing custom logic for Optimism Prime project.

Join us on discord for more details: https://discord.gg/m3Na57wfq4

## Local setup

Clone the project and install dependencies with:

```bash
pnpm install
```

You can run the tests with:

```bash
pnpm test
```

Take a look at `package.json` for more scripts.
