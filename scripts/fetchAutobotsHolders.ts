import { ethers } from "hardhat";

const OPTIMISM_CHAIN_ID = 10;
const AUTOBOT = "0xA0B15044bB7b4eAfd9b34180BfE727f4cd949300";

const legendaries = [768, 940, 512, 256];
const goldens = [
  15, 24, 62, 79, 80, 85, 115, 169, 189, 210, 214, 248, 274, 276, 282, 296, 306,
  349, 396, 398, 421, 422, 438, 489, 502, 509, 540, 547, 557, 562, 565, 582,
  590, 609, 637, 685, 712, 747, 781, 836, 837, 846, 853, 905, 921, 927, 935,
  937, 938, 939,
];

async function main() {
  if (OPTIMISM_CHAIN_ID != (await ethers.provider.getNetwork()).chainId) {
    throw new Error("This script should be run on optimism (chainId = 10)");
  }
  const autobot = new ethers.Contract(
    AUTOBOT,
    ["function ownerOf(uint256) view returns (address)"],
    ethers.provider,
  );
  const ownerOf: { [key: string]: string } = {};
  console.log("Legendaries:");
  for (const id of legendaries) {
    ownerOf[`${id}`] = await autobot.ownerOf(id);
    console.log(`${ownerOf[id]} ${id}`);
  }
  console.log("\nGoldens:");
  for (const id of goldens) {
    ownerOf[id] = await autobot.ownerOf(id);
    console.log(`${ownerOf[id]} ${id}`);
  }
  console.log("\n");
  console.log(ownerOf);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
