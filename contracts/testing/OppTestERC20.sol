// SPDX-License-Identifier: Unlicense

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract OppTestERC20 is Ownable, ERC20 {
  constructor() ERC20("Optimism Prime Test Token", "OppTestERC20") {
    _mint(msg.sender, 1_000_000_000);
  }

  function mint(uint256 amount) external onlyOwner {
    _mint(msg.sender, amount);
  }

  function mintTo(uint256 amount, address recipient) external onlyOwner {
    _mint(recipient, amount);
  }
}
