// SPDX-License-Identifier: Unlicense

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

// This contract allows to distribute rewards as any tokens to Golden/Legendary Autobots holders.
// Workflow: send tokens to this contract and then call distribute with the token address
contract AutobotRevenueDistributor is Ownable {
  IERC721 public constant AUTOBOTS = IERC721(0xA0B15044bB7b4eAfd9b34180BfE727f4cd949300);

  uint256 public constant RATE_DIVIDER = 1000;
  uint256 public goldenRate = 750;
  uint256 public legendaryRate = 250;

  uint256[] public goldenIds = [
    15, 24, 62, 79, 80, 85, 115, 169, 189, 210, 214, 248, 274, 276, 282, 296, 306,
    349, 396, 398, 421, 422, 438, 489, 502, 509, 540, 547, 557, 562, 565, 582,
    590, 609, 637, 685, 712, 747, 781, 836, 837, 846, 853, 905, 921, 927, 935,
    937, 938, 939
  ];
  uint256[] public legendaryIds = [768, 940, 512, 256];

  function setGoldenRate(uint256 _newRate) external onlyOwner {
    goldenRate = _newRate;
  }

  function setLegendaryRate(uint256 _newRate) external onlyOwner {
    legendaryRate = _newRate;
  }

  function withdraw(IERC20 token) external onlyOwner {
    token.transfer(msg.sender, token.balanceOf(address(this)));
  }

  function legendaryIdsLength() public view returns (uint256) {
    return legendaryIds.length;
  }

  function goldenIdsLength() public view returns (uint256) {
    return goldenIds.length;
  }

  function distribute(IERC20 token) public {
    uint256 totalAmount = token.balanceOf(address(this));

    uint256 goldenAmount = totalAmount * goldenRate / RATE_DIVIDER;
    _distribute(token, goldenAmount, goldenIds);

    uint256 legendaryAmount = totalAmount * legendaryRate / RATE_DIVIDER;
    _distribute(token, legendaryAmount, legendaryIds);
  }

  function _distribute(IERC20 token, uint256 amount, uint256[] memory tokenIds) private {
    uint256 amountPerNFT = amount / tokenIds.length;
    require(amountPerNFT > 0, "Amount to distribute is too low");
    for(uint256 idx = 0; idx < tokenIds.length; ++idx) {
      address owner = AUTOBOTS.ownerOf(tokenIds[idx]);
      token.transfer(owner, amountPerNFT);
    }
  }
}
